## Identifying Information

* Perat Damrongsiri

* peratd@uoregon.edu

## Calculation

I use your provided acp_times_solved.py. Min speed will be 15 km/h from 0 - 200 km, 15 km/h from 200 - 400 km, 15 km/h from 400 - 600 km, 11.428 km/h from 600 - 1000 km, and 13.333 km/h from 1000 - 1300 km. Max speed will be 34 km/h from 0 - 200 km, 32 km/h from 200 - 400 km, 30 km/h from 400 - 600 km, 28 km/h from 600 - 1000 km, and 28 km/h from 1000 - 1300 km. Final control times at or exceeding brevet distance are 13.5 h at 200 km, 20 h at 300 km, 27 h at 400 km, 40 h at 600 km, and 75 h at 1000 km. Maximum distance will be 5% more than the control distance.

* Calculation Example: at 500 km

* min time = 200/34 + 200/32 + 100/30

## Test Cases

* Put any character (such as 'abc/+.%>?') that is not a number or floating point number into the input field for either miles or km (The program will erase it out automatically and put an error message says that "Input is not a number!" on note field)

* Put duplicate distance (such as put 0 km into 2 rows) into the input field for either miles or km (The program will erase it out automatically and put an error message says that "Cannot have duplicate distance!" on note field)

* Skip 1 or more row(s) when put the distance into either miles or km input field (put 0 in the first row and skip the next row then put 1 into the third row) (The program will erase it and put an error message says that ""Previous input box is empty!" on note field)

* Put in a brevet distance that is furthur than 1.05 times of the control distance (put 211 km when control distance is 200 km)

* Click Display button before submit anything (The new tab should shows up a page says that the database is empty)

* Click submit button when there is nothing in the input box should not show any error